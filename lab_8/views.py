from django.shortcuts import render

# Create your views here.
response={}
def index(request):
    response['author'] = "Jeffry Aldi Permana"
    html = 'lab_8/lab_8.html'
    return render(request, html, response)
