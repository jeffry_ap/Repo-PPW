$(document).ready(function () {
  themes = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
  {"id":0,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
  {"id":0,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
  {"id":0,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
  {"id":0,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
  {"id":0,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
  {"id":0,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
  {"id":0,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
  {"id":0,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
  {"id":0,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
  {"id":0,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}];

  localStorage.setItem("themes",JSON.stringify(themes));

  mySelect = $('.my-select').select2();

  mySelect.select2({
    'data': JSON.parse(localStorage.getItem("themes")
    )
  });

  var arraysOfTheme = JSON.parse(localStorage.getItem("themes"));
  var indigoTheme = arraysOfTheme[3];
  var defaultTheme = indigoTheme;
  var selectedTheme = defaultTheme;
  var chacedTheme = defaultTheme;

  if (localStorage.getItem("yangDipilih")!==null) {
    perubahan = JSON.parse(localStorage.getItem("yangDipilih"));
  }

  yangDipilih = perubahan;

  $('body').css(
        {
            "background-color": yangdipilih.bcgColor,
            "font-color": yangdipilih.fontColor
        }
    );
});

function Submit(e){
	var key=e.keyCode;
	if (key == 13) {
		var text = document.getElementById("teks").value;
    if (text.length == 0 || text = "") {
      alert("Text belum diisi!");
    }
    else{
      document.getElementById("tempat").innerHTML += "You:<br>"+text+"<br>";  
    }
		document.getElementById("teks").value = "";
	}
}