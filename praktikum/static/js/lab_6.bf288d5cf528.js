$(document).ready(function () {
  themes = [{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
  {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
  {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
  {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
  {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
  {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
  {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
  {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
  {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
  {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
  {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}];

  localStorage.setItem("themes",JSON.stringify(themes));

  mySelect = $('.my-select').select2();

  mySelect.select2({
    'data': JSON.parse(localStorage.getItem("themes")
    )
  });

  var arraysOfTheme = JSON.parse(localStorage.getItem("themes"));
  var indigoTheme = arraysOfTheme[3];
  var defaultTheme = indigoTheme;
  var selectedTheme = defaultTheme;
  var chacedTheme = defaultTheme;

  $("#arrow").click(function() {
    $(".chat-body").toggle();
  });

  $('#apply').on('click', function(){ 
    var value = mySelect.val();
    
    if(value < arraysOfTheme.length){
      chosenTheme = arraysOfTheme[value];
      currentTheme = chosenTheme;
  }
    
  $('body').css({
      "background-color": currentTheme.bcgColor,
    "font-color":currentTheme.fontColor
  });
  
  localStorage.getItem("currentTheme", JSON.stringify(currentTheme));
});
});


function Submit(e){
	var key=e.keyCode;
	if (key == 13) {
		var text = document.getElementById("teks").value;
    if (text.length == 0 || text == "") {
      alert("Text belum diisi!");
    }
    else{
      document.getElementById("tempat").innerHTML += "You:<br>"+text+"<br>";  
    }
		document.getElementById("teks").value = "";
	}
}